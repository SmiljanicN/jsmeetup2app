import { Dimensions, Platform } from 'react-native';

export const getScreenWidth = () => Dimensions.get('window').width;
export const getScreenHeight = () => Dimensions.get('window').height;

export const isAndroid = Platform.OS === 'android';
export const isIos = Platform.OS === 'ios';
