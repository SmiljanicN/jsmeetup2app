import React, { PureComponent } from 'react';
import { SearchBar } from 'react-native-elements';

export default class DemoSearch extends PureComponent {
  state = {
    search: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  render() {
    const { search } = this.state;

    return (
      <SearchBar
        platform="ios"
        placeholder="Type Here..."
        onChangeText={this.updateSearch}
        value={search}
        containerStyle={{ width: 340 }}
      />
    );
  }
}