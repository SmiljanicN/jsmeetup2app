import React  from 'react';

import { StyleSheet, Text } from 'react-native'
import { ListItem } from 'react-native-elements';

import { Gray, BorderRadius } from '../styles';

const DemoListItem = () => {
  return (
    <ListItem
      leftAvatar={{
        title: 'AB',
        source: { uri: 'https://picsum.photos/id/674/80' },
        showEditButton: true,
      }}
      title="Albert C. Barnum"
      subtitle="3506 Archwood Avenue"
      containerStyle={styles.List}
      chevron
    />
  );
}

const styles = StyleSheet.create({
  List: {
    width: 320,
    borderRadius: BorderRadius,
    borderColor: Gray,
    borderWidth: 1,
    marginBottom: 10,
  },
});

export { DemoListItem };