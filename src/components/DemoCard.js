import React  from 'react';

import { StyleSheet, Text } from 'react-native'
import { Card, Button, Icon } from 'react-native-elements';

import { Gray, BorderRadius, MarginDefault } from '../styles';

const DemoCard = () => {
  return (
    <Card
      title='TITLE'
      image={{ uri: 'https://picsum.photos/200/300' }}
      containerStyle={styles.Card}
      >
      <Text>
        The idea with React Native Elements is more about component structure than actual design.
      </Text>
      <Button
        icon={<Icon name='code' color='#ffffff' />}
        buttonStyle={styles.Button}
        titleStyle={styles.ButtonLabel}
        title='BUTTON' />
    </Card>
  );
}

const styles = StyleSheet.create({
  Card: {
    borderRadius: BorderRadius,
    borderColor: Gray,
    borderWidth: 1,
  },
  Button: {
    borderRadius: BorderRadius,
    marginTop: MarginDefault,
  },
  ButtonLabel: {
    fontSize: 16,
    marginLeft: MarginDefault,
    fontWeight: 'bold',
  },
});

export { DemoCard };