import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';

import { createBottomTabNavigator } from 'react-navigation-tabs';

import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import Splash from '../screens/Splash';
import Home from '../screens/Home';

import { DEFAULT_HEADER_STLYE, DEFAULT_TAB_NAVIGATION_STYLE } from '../styles';

// Dummy screen
class Program extends PureComponent {
  static navigationOptions = {
    title: 'Program!',
  };

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Program!</Text>
      </View>
    );
  }
}

// Dummy screen
class Settings extends PureComponent {
  static navigationOptions = {
    title: 'Settings',
  };

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!</Text>
      </View>
    );
  }
}

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: Home,
    },
  },
  {
    initialRouteName: 'Home',
    ...DEFAULT_HEADER_STLYE,
  }
);

const SettingsStack = createStackNavigator(
  {
    Settings: {
      screen: Settings,
    },
  },
  {
    initialRouteName: 'Settings',
    ...DEFAULT_HEADER_STLYE,
  }
);

const ProgramStack = createStackNavigator(
  {
    Program: {
      screen: Program,
    },
  },
  {
    initialRouteName: 'Program',
    ...DEFAULT_HEADER_STLYE,
  }
);

const TabNavigator = createBottomTabNavigator({
  Home: HomeStack,
  Program: ProgramStack,
  Settings: SettingsStack,
}, DEFAULT_TAB_NAVIGATION_STYLE);

const AppNavigator = createSwitchNavigator({
  Splash: Splash,
  App: TabNavigator,
}, {
  initialRouteName: 'Splash',
});

export default createAppContainer(AppNavigator);
