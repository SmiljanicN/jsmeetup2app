import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';

import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';

import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import Icon from 'react-native-vector-icons/Ionicons';

import Splash from '../screens/Splash';
import Home from '../screens/Home';

import { DEFAULT_HEADER_STLYE, DEFAULT_TAB_NAVIGATION_STYLE, LeviBlue, Gray } from '../styles';

// Dummy screen
class Program extends PureComponent {
  static navigationOptions = {
    title: 'Program!',
  };

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Program!</Text>
      </View>
    );
  }
}

// Dummy screen
class Settings extends PureComponent {
  static navigationOptions = {
    title: 'Settings',
  };

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings!</Text>
      </View>
    );
  }
}

// Dummy screen
class Profile extends PureComponent {
  static navigationOptions = {
    title: 'Profile!',
  };

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>>>>> Profile!</Text>
      </View>
    );
  }
}

const HomeStack = createStackNavigator(
  {
    Home: {
      screen: Home,
    },
  },
  {
    initialRouteName: 'Home',
    ...DEFAULT_HEADER_STLYE,
  }
);

const ProgramStack = createStackNavigator(
  {
    Program: {
      screen: Program,
    },
  },
  {
    initialRouteName: 'Program',
    ...DEFAULT_HEADER_STLYE,
  }
);

const SettingsStack = createStackNavigator(
  {
    Settings: {
      screen: Settings,
    },
  },
  {
    initialRouteName: 'Settings',
    ...DEFAULT_HEADER_STLYE,
  }
);

const ProfileStack = createStackNavigator(
  {
    Profile: {
      screen: Profile,
    }
  },
  {
    initialRouteName: 'Profile',
    ...DEFAULT_HEADER_STLYE,
  }
);

const TabNavigator = createBottomTabNavigator({
  Home: {
    screen: HomeStack,
  },
  Program: {
    screen: ProgramStack,
  },
  Settings: {
    screen: SettingsStack,
  },
}, {
  ...DEFAULT_TAB_NAVIGATION_STYLE,
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ tintColor }) => {
      const { routeName } = navigation.state;
      let IconComponent = Icon;
      let iconName;

      // https://ionicframework.com/docs/v3/ionicons/
      if (routeName === 'Home') {
        iconName = 'ios-home';
      } else if (routeName === 'Program') {
        iconName = 'ios-albums';
      } else if (routeName === 'Settings') {
        iconName = 'ios-options';
      }
      return <IconComponent name={iconName} size={22} color={tintColor} />;
    },
  }),
  tabBarOptions: {
    activeTintColor: LeviBlue,
    inactiveTintColor: Gray,
  },
});

const DrawerNavigator = createDrawerNavigator({
  Dashboard: TabNavigator,
  Profile: ProfileStack,
}, {
  initialRouteName: 'Dashboard',
});

const AppNavigator = createSwitchNavigator({
  Splash: Splash,
  App: DrawerNavigator,
}, {
  initialRouteName: 'Splash',
});

export default createAppContainer(AppNavigator);
