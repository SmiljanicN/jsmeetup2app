import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';

import Splash from '../screens/Splash';
import Home from '../screens/Home';

import { DEFAULT_HEADER_STLYE } from '../styles';

const StackNavigation = createStackNavigator(
  {
    Splash: {
      screen: Splash,
      navigationOptions: {
        gesturesEnabled: false,
        headerStyle: {
          backgroundColor: 'transparent',
        },
      },
    },
    Home: {
      screen: Home,
    },
  },
  {
    initialRouteName: 'Splash',
    ...DEFAULT_HEADER_STLYE,
  }
);

export default createAppContainer(StackNavigation);
