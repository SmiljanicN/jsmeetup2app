import { combineReducers } from 'redux';
import deviceReducer from './DeviceReducer';

// Root Reducer
const rootReducer = combineReducers({
  device: deviceReducer,
});

export default rootReducer;
