import { CONECTION } from '../actions/Types';

const INITIAL_STATE = {
  isConnected: true,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CONECTION:
      return { ...state, isConnected: action.payload };

    default:
      return state;
  }
};

export const getIsConnected = state => state.device.isConnected;
