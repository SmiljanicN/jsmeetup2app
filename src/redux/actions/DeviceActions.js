import { CONECTION } from './Types';

export const setConnectionInfo = hasInternet => {
  return {
    type: CONECTION,
    payload: hasInternet,
  };
};
