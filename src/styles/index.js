export const White = '#FFFFFF';
export const Black = '#333333';
export const LeviBlue = '#1D4087';
export const LightGray = '#F5F6F8';
export const Gray = '#CBCBD7';

export const CorrectGreen = '#80B03D';
export const ErrorRed = '#cc0000';

export const MarginDefault = 10;
export const PaddingDefault = 10;
export const BorderRadius = 10;

export const DEFAULT_HEADER_STLYE = {
  defaultNavigationOptions: {
    headerStyle: {
      backgroundColor: LeviBlue,
    },
    headerTintColor: 'white',
    headerTitleStyle: {
      fontWeight: 'bold',
      fontSize: 16
    },
    headerBackTitle: 'Back',
    headerBackTitleStyle: {
      fontSize: 14
    }
  }
};

export const DEFAULT_TAB_NAVIGATION_STYLE = {
  tabBarOptions: {
    activeTintColor: LeviBlue,
    labelStyle: {
      fontSize: 12,
      fontWeight: 'bold',
    },
    style: {
      backgroundColor: LightGray,
    },
  },
};