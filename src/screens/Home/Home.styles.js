import { StyleSheet } from 'react-native';
import { LightGray, PaddingDefault } from '../../styles';

const styles = StyleSheet.create({
  Screen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: LightGray,
  },
  ScrollView: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingTop: PaddingDefault,
  },
  ListWrapper: {
    marginVertical: 20,
  },
});

export default styles;