// import React, { PureComponent } from 'react';
// import { View, Text, StatusBar } from 'react-native';

// import { White, LeviBlue } from '../../styles';

// import styles from './Home.styles';

// export default class Home extends PureComponent {
//   static navigationOptions = {
//     title: 'Home Title',
//   };

//   render() {
//     return (
//       <View style={styles.Screen}>
//         <Text>Home screen!</Text>
//       </View>
//     );
//   }
// }



/*********************************************************
DEMO CODE - CUSTOM COMPONENTS
************************************************************/
import React, { PureComponent } from 'react';
import { ScrollView, View, Text, StatusBar } from 'react-native';

import { DemoCard } from '../../components/DemoCard';
import DemoSearch from '../../components/DemoSearch';
import { DemoListItem } from '../../components/DemoListItem';

import { White, LeviBlue } from '../../styles';

import styles from './Home.styles';

export default class Home extends PureComponent {
  static navigationOptions = {
    title: 'Home Title',
  };

  render() {
    return (
      <View style={styles.Screen}>
        <StatusBar backgroundColor={LeviBlue} barStyle="light-content" />
        <ScrollView contentContainerStyle={[styles.ScrollView]}>
          <DemoSearch />
          <DemoCard />
          <View style={styles.ListWrapper}>
            <DemoListItem />
            <DemoListItem />
            <DemoListItem />
            <DemoListItem />
          </View>
        </ScrollView>
      </View>
    );
  }
}
