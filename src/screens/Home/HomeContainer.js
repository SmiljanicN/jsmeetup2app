// import Home from './Home';

// export default Home;


/*********************************************************
DEMO CODE - REDUX
************************************************************/
import { connect } from 'react-redux';

import { getIsConnected } from '../../redux/reducers/DeviceReducer';

import Home from './Home';

const mapStateToProps = state => ({
  isConnected: getIsConnected(state),
});

export default connect(mapStateToProps)(Home);
