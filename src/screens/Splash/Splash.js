import React, { PureComponent } from 'react';
import { View, Image, ActivityIndicator, StatusBar } from 'react-native';

import { White, LeviBlue } from '../../styles';

import styles from './Splash.styles';

export default class Splash extends PureComponent {
  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    const { navigation } = this.props;
    
    this.loadingTimer = null;

    /*********************************************************
    DEMO CODE - STACK NAVIGATION SCREENS
    ************************************************************/
    // this.loadingTimer = setTimeout(() => {
    //   navigation.replace('Home');
    // }, 3000);

    /*********************************************************
    DEMO CODE - TAB AND DRAWER NAVIGATION SCREENS
    ************************************************************/
    this.loadingTimer = setTimeout(() => {
      navigation.navigate('App');
    }, 3000);
  }

  componentWillUnmount() {
    clearTimeout(this.loadingTimer);
  }

  render() {

    return (
      <View style={styles.Screen}>
        <StatusBar backgroundColor={LeviBlue} barStyle="light-content" />
        <Image 
          source={require('../../assets/img/levi9-logo.png')}
          style={styles.SplashLogo} 
        />
        <ActivityIndicator size="large" color={White} style={styles.Loader}/>
      </View>
    );
  }
}