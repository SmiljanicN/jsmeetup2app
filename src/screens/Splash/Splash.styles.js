import { StyleSheet } from 'react-native';
import { LeviBlue, MarginDefault } from '../../styles';

const styles = StyleSheet.create({
  Screen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: LeviBlue,
  },
  SplashLogo: {
    width: '60%',
    resizeMode: 'contain',
  },
  Loader: {
    position: 'absolute',
    bottom: MarginDefault * 4,
    left: 0,
    right: 0,
    margin: 'auto',
  }
});

export default styles;